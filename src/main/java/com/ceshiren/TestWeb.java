package com.ceshiren;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class TestWeb {
    SimpleDateFormat df = new SimpleDateFormat("HH.mm.ss");//设置日期格式


    private static WebDriver driver;

    @Test
    void testSearch(){
        System.setProperty("webdriver.chrome.driver", "E:\\testerhome\\selenium_webdriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        driver.get("http://ceshiren.com");
        driver.findElement(By.cssSelector("#search-button")).click();
        driver.findElement(By.cssSelector("#search-term")).sendKeys("123");
    }

    @Test
    static void needLogin() throws IOException, InterruptedException {
        System.setProperty("webdriver.chrome.driver", "E:\\testerhome\\selenium_webdriver\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.get("https://work.weixin.qq.com/wework_admin/frame");

        Thread.sleep(15000);
        Set<Cookie> cookies = driver.manage().getCookies();
        ObjectMapper mapper=new ObjectMapper(new YAMLFactory());
        mapper.writeValue(new File("cookies.yml"),cookies);

    }

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        File file=new File("cookies.yml");
        if (file.exists()) {
            System.setProperty("webdriver.chrome.driver", "E:\\testerhome\\selenium_webdriver\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.get("https://work.weixin.qq.com/wework_admin/frame");
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            TypeReference<List<HashMap<String, Object>>> typeReference = new TypeReference<List<HashMap<String, Object>>>() {
            };

            List<HashMap<String, Object>> cookies = mapper.readValue(new File("cookies.yml"), typeReference);

            System.out.println(cookies);
            cookies.forEach(cookieMap -> {
                driver.manage().addCookie(new Cookie(cookieMap.get("name").toString(), cookieMap.get("value").toString()));
            });
            driver.navigate().refresh();
        }
        else {
            needLogin();
        }
    }

    @Test
    void addMember(){
        String name="test"+df.format(new Date());
        //driver.get("https://work.weixin.qq.com/wework_admin/frame#index");
        driver.findElement(By.cssSelector("[node-type='addmember']")).click();
        driver.findElement(By.name("username")).sendKeys(name);
        driver.findElement(By.name("acctid")).sendKeys(name);
        driver.findElement(By.name("alias")).sendKeys(name+"@123.com");

        driver.findElement(By.linkText("保存")).click();
    }

    @Test
    void searchMember(){
        doClick(By.id("menu_contacts"));
        doInput(By.id("memberSearchInput"),"test1");
        doClick(By.xpath("//*[@id='search_party_list' and @title='sjw/test1']"));
        String contents=driver.findElement(By.cssSelector(".js_party_info")).getText();
        System.out.println(contents);
        assert contents.contains("无任何成员");


    }

    static void doClick(By by){
        driver.findElement(by).click();
    }

    static void doInput(By by, String content){
        driver.findElement(by).sendKeys(content);
    }
}
